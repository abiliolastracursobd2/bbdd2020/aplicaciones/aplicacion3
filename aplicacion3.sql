﻿

DROP TABLE IF EXISTS coches;
CREATE TABLE
  coches(
  ID int,
  marca varchar(50),
  fecha date,
  precio float,
  PRIMARY KEY  (ID)
  );
DROP TABLE IF EXISTS clientes;
CREATE TABLE clientes(
  cod int,
  nombre varchar(50),
  id_coche_alquilado int,
  fecha_alquiler date,
  PRIMARY KEY(cod)
  );
ALTER TABLE clientes
  ADD CONSTRAINT fkclientcoche FOREIGN KEY (id_coche_alquilado)
  REFERENCES coches(id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT uk_clientes UNIQUE (id_coche_alquilado);
  