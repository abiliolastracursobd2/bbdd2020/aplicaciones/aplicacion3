<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property int|null $id_coche_alquilado
 * @property string|null $fecha_alquiler
 *
 * @property Coches $cocheAlquilado
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'required'],
            [['cod', 'id_coche_alquilado'], 'integer'],
            [['fecha_alquiler'], 'safe'],
            [['nombre'], 'string', 'max' => 50],
            [['id_coche_alquilado'], 'unique'],
            [['cod'], 'unique'],
            [['id_coche_alquilado'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::className(), 'targetAttribute' => ['id_coche_alquilado' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'id_coche_alquilado' => 'Id Coche Alquilado',
            'fecha_alquiler' => 'Fecha Alquiler',
        ];
    }

    /**
     * Gets query for [[CocheAlquilado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCocheAlquilado()
    {
        return $this->hasOne(Coches::className(), ['ID' => 'id_coche_alquilado']);
    }
}
