<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coches".
 *
 * @property int $ID
 * @property string|null $marca
 * @property string|null $fecha
 * @property float|null $precio
 *
 * @property Clientes $clientes
 */
class Coches extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID'], 'integer'],
            [['fecha'], 'safe'],
            [['precio'], 'number'],
            [['marca'], 'string', 'max' => 50],
            [['ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'marca' => 'Marca',
            'fecha' => 'Fecha',
            'precio' => 'Precio',
        ];
    }

    /**
     * Gets query for [[Clientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasOne(Clientes::className(), ['id_coche_alquilado' => 'ID']);
    }
}
